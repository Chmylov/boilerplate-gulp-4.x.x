/* 
Gulp 4

Commands: `npm run serve` or `npm run build`

1. `npm run serve`: Will start local server end 
compile all files not minified with sourcemaps.

2. `npm run build`: Will build files 
for production, minified and without sourcemaps.

You can run gulp without `npm run **` command.
Just install gulp global and use for example `gulp build`
*/

const { watch, series, parallel, src, dest } = require('gulp');
const flatten = require('gulp-flatten');
const sass = require('gulp-sass');
const sourcemaps = require('gulp-sourcemaps');
const del = require('del');
const ifElse = require('gulp-if-else');
const cleanCSS = require('gulp-clean-css');
const babel = require('gulp-babel');
const browserSync = require('browser-sync').create();
const rollup = require('gulp-rollup');
const rename = require('gulp-rename');
const uglify = require('gulp-uglify');

// Enable or disable development
let isDev = process.argv.includes('--dev');

// Start browser sync
function serve() {
    browserSync.init({
        server: {
            baseDir: './dist'
        }
    });
}

// Clean up dist folder
function clean() {
    return del('dist/*');
}

// Copy html to dist folder
function html() {
    return src('src/html/**/*.html')
        .pipe(flatten())
        .pipe(dest('dist'))
        .pipe(browserSync.stream());
}

// Compile css
function css() {
    return (
        src(['src/scss/main.scss'])
            // .pipe(rename({ extname: '.scss' }))
            .pipe(ifElse(isDev, () => sourcemaps.init()))
            .pipe(sass().on('error', sass.logError))
            // .pipe(concat('main.css'))
            .pipe(ifElse(isDev, () => sourcemaps.write()))
            .pipe(ifElse(!isDev, () => cleanCSS()))
            .pipe(dest('dist/css'))
            .pipe(browserSync.stream())
    );
}

// This is needed for js compile
function getBabelConfig() {
    let babelConfig = {
        presets: [
            [
                '@babel/preset-env',
                {
                    modules: 'false'
                }
            ]
        ]
    };

    if (!isDev) babelConfig.presets.push(['minify']);

    return babelConfig;
}

// Compile js
function js() {
    return src('src/js/**/*.js')
        .pipe(ifElse(isDev, () => sourcemaps.init()))
        .pipe(babel(getBabelConfig()))
        .pipe(ifElse(isDev, () => sourcemaps.write()))
        .pipe(dest('dist/js'))
        .pipe(browserSync.stream());
}

// Build js bundle for old browsers
function jsBundle() {
    return src('./dist/js/**/*.js')
        .pipe(ifElse(isDev, () => sourcemaps.init()))
        .pipe(
            rollup({
                input: './dist/js/main.js',
                output: {
                    format: 'cjs'
                }
            })
        )
        .pipe(rename({ basename: 'bundle' }))
        .pipe(ifElse(isDev, () => sourcemaps.write()))
        .pipe(ifElse(!isDev, () => uglify()))
        .pipe(dest('./dist/js'));
}

// Copy images to the dist folder
function images() {
    return src('src/images/**/*')
        .pipe(flatten())
        .pipe(dest('dist/images'))
        .pipe(browserSync.stream());
}

// Run and build everything
function build() {
    return series(clean, html, css, js, jsBundle, images);
}

// Watch files for changes
function watchSrc() {
    watch('src/html/*', html);
    watch('src/scss/*', css);
    watch('src/js/*', js);
}

exports.watch = series(build(), parallel(watchSrc, serve));
exports.default = build();

// That is all, bye
