function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

var Example =
/*#__PURE__*/
function () {
  function Example() {
    _classCallCheck(this, Example);
  }

  _createClass(Example, [{
    key: "sayHallo",
    value: function sayHallo() {
      console.log('Hallo world from JS!');
    }
  }]);

  return Example;
}();

export { Example };
//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIm1vZHVsZS5qcyJdLCJuYW1lcyI6WyJFeGFtcGxlIiwiY29uc29sZSIsImxvZyJdLCJtYXBwaW5ncyI6Ijs7Ozs7O0lBQU1BLE87OztBQUNGLHFCQUFjO0FBQUE7QUFBRTs7OzsrQkFFTDtBQUNQQyxNQUFBQSxPQUFPLENBQUNDLEdBQVIsQ0FBWSxzQkFBWjtBQUNIOzs7Ozs7QUFHTCxTQUFTRixPQUFUIiwic291cmNlc0NvbnRlbnQiOlsiY2xhc3MgRXhhbXBsZSB7XHJcbiAgICBjb25zdHJ1Y3RvcigpIHt9XHJcblxyXG4gICAgc2F5SGFsbG8oKSB7XHJcbiAgICAgICAgY29uc29sZS5sb2coJ0hhbGxvIHdvcmxkIGZyb20gSlMhJyk7XHJcbiAgICB9XHJcbn1cclxuXHJcbmV4cG9ydCB7IEV4YW1wbGUgfTtcclxuIl0sImZpbGUiOiJtb2R1bGUuanMifQ==
